package id.co.roxas.logger.constant;

public class AppConstants {
	 	public static final String TOPIC_NAME = "test";
	 	public static final String TOPIC_LOGGER = "logger";
	    public static final String GROUP_ID = "group_id";
}
