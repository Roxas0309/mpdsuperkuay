package id.co.roxas.logger.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.roxas.logger.entity.LoggerApplicationWs;

@Repository
public interface LoggerApplicatiobWsDao extends JpaRepository<LoggerApplicationWs, Long>{

}
