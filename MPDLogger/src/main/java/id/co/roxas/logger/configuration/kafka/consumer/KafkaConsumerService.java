package id.co.roxas.logger.configuration.kafka.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import id.co.roxas.logger.BaseCommon;
import id.co.roxas.logger.constant.AppConstants;
import id.co.roxas.logger.dao.LoggerApplicatiobWsDao;
import id.co.roxas.logger.entity.LoggerApplicationWs;

@Service
public class KafkaConsumerService extends BaseCommon{
    private final Logger logger = 
            LoggerFactory.getLogger(KafkaConsumerService.class);
 
    @Autowired
    private LoggerApplicatiobWsDao loggerApplicatiobWsDao;
    
    @KafkaListener(topics = AppConstants.TOPIC_NAME, 
            groupId = AppConstants.GROUP_ID)
    public void consume(String message) 
    {
        logger.info(String.format("Message recieved From Kafka Logger Dunks-> %s", message));
    }
    
    @KafkaListener(topics = AppConstants.TOPIC_LOGGER, 
            groupId = AppConstants.GROUP_ID)
    public void consumeLogger(String message) 
    {
    	LoggerApplicationWs applicationWs ;
		try {
			applicationWs = mapperJsonToSingleDto(message, LoggerApplicationWs.class);
			logger.info("applicationWs : " + new Gson().toJson(applicationWs));
			loggerApplicatiobWsDao.save(applicationWs);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}