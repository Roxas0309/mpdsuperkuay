package id.co.roxas.logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MpdLoggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MpdLoggerApplication.class, args);
	}

}
