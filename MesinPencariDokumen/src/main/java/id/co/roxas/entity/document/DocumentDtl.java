package id.co.roxas.entity.document;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table
public class DocumentDtl {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	Long id;

	@Column(name = "id_hdr")
	Long idHdr;

	@Column(columnDefinition="TEXT")
	String wordingPerLine;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	Date createdDate;

	@Column(name = "is_searchable")
	Boolean isSearchable;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdHdr() {
		return idHdr;
	}

	public void setIdHdr(Long idHdr) {
		this.idHdr = idHdr;
	}

	public String getWordingPerLine() {
		return wordingPerLine;
	}

	public void setWordingPerLine(String wordingPerLine) {
		this.wordingPerLine = wordingPerLine;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsSearchable() {
		return isSearchable;
	}

	public void setIsSearchable(Boolean isSearchable) {
		this.isSearchable = isSearchable;
	}
	
	

}
