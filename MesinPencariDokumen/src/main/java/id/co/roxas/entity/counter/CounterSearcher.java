package id.co.roxas.entity.counter;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class CounterSearcher {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	Long id;
	
	@Column(name="search_word")
	String searchWord;
	
	@Column(name="counter_search")
	BigInteger counterSearch;
	
	@Column(name="user_id_accessor")
	Long userIdAccessor; 
	
	

	public Long getUserIdAccessor() {
		return userIdAccessor;
	}

	public void setUserIdAccessor(Long userIdAccessor) {
		this.userIdAccessor = userIdAccessor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public BigInteger getCounterSearch() {
		return counterSearch;
	}

	public void setCounterSearch(BigInteger counterSearch) {
		this.counterSearch = counterSearch;
	}
	
	
}
