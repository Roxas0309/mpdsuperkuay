package id.co.roxas.entity.query;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table
@NamedNativeQueries(value= {
		@NamedNativeQuery(name="QuerySelectorDocument.findQueryAllByUserAccessor", 
				query = "SELECT  qsd.query \n"
						+ "FROM query_selector_document qsd where qsd.user_accessor = ?1 group by query")
})
public class QuerySelectorDocument {


	   @Id
	   @GeneratedValue
	   Integer id;
	   
	   String query;
	   
	   String userAccessor;
		
	   @Temporal(TemporalType.TIMESTAMP)
	   @Column(name = "created_date")
		Date createdDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getUserAccessor() {
		return userAccessor;
	}

	public void setUserAccessor(String userAccessor) {
		this.userAccessor = userAccessor;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	   
	
	   
}
