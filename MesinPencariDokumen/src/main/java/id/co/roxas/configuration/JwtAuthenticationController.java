package id.co.roxas.configuration;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import id.co.roxas.configuration.kafka.producer.KafKaProducerService;
import id.co.roxas.dao.user.UserRepDao;
import id.co.roxas.dto.LoggerApplicationWs;
import id.co.roxas.entity.user.UserRep;
import id.co.roxas.service.AuthenticationSvc;
import id.co.roxas.throwable.NotFoundDataException;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Value("${secret.key}")
	private String secretKey;
	
	@Value("${secret.id}")
	private String secretId;
	
	@Value("${admin.id}")
	private String adminId;
	
	@Value("${admin.key}")
	private String adminKey;

	@Autowired
	private AuthenticationSvc authenticationSvc;
	
	@Autowired 
	private KafKaProducerService kafKaProducerService;
	
	private String BycriptMe(String password) {
		System.out.println("password adalah sebelun" + password);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(password);
		System.out.println("password adalah sesudah" + encodedPassword);
		return encodedPassword;
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
		String password = authenticationRequest.getPassword();
		String userName = authenticationRequest.getUsername();
		UserRep userRepDao = authenticationSvc.getUserRep(password, userName);
		LoggerApplicationWs loggerApplicationWs = new LoggerApplicationWs();
		loggerApplicationWs.setCreatedDate(new Date());
		loggerApplicationWs.setRequestBody(new Gson().toJson(authenticationRequest));
		loggerApplicationWs.setUrl("/login");
		if(userRepDao == null && (!password.equals(adminKey) || !userName.equals(adminId))) {
			loggerApplicationWs.setResponseBody("Username atau Password Tidak Ada");
			kafKaProducerService.sendMessageLogger(loggerApplicationWs);
			throw new NotFoundDataException("Username atau Password Tidak Ada");
		}
		
		authenticate(userName,secretId);    
		final UserDetails userDetails = new JwtUserDetailsService(authenticationRequest.getUsername(),password);
		final String token = jwtTokenUtil.generateToken(userDetails);
		//kafKaProducerService.sendMessage("User Login Dengan Data Sebagai Berikut " + new Gson().toJson(authenticationRequest));
		loggerApplicationWs.setResponseBody("Berhasil Login");
		kafKaProducerService.sendMessageLogger(loggerApplicationWs);
		return ResponseEntity.ok(new JwtResponse(token));
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
	
}
