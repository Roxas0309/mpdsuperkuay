package id.co.roxas.configuration.cache.service;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import id.co.roxas.constant.AppConstants;
import id.co.roxas.dao.user.UserRepDao;
import id.co.roxas.entity.query.QuerySelectorDocument;
import id.co.roxas.entity.user.UserRep;
import id.co.roxas.throwable.MultipleDataErrorException;
import id.co.roxas.throwable.NotFoundDataException;

@Service
public class UserCacheService {

	@Autowired
	private UserRepDao userRepDao;
	
	    @CacheEvict(value=AppConstants.CACHE_GROUP.USER_SELECTOR, allEntries = true)
	    public UserRep savePic(String userId) {
		  	UserRep userRep = userRepDao.findByUserId(userId);
//		  	System.out.println("user rep adalah " + new Gson().toJson(userRep.getUsername()));
		  	if(userRep!=null && Strings.isNotEmpty(userRep.getUsername())) {
		  		throw new MultipleDataErrorException("Id Sudah Ditemukan");
		  	}
		  	userRep = new UserRep();
		  	userRep.setCreatedDate(new Date());
		  	userRep.setIsActive(true);
		  	userRep.setPassword("Roxas0309.");
		  	userRep.setUsername(userId);
		  	userRepDao.save(userRep);
	        return userRep;
	    }
	    
	    @CacheEvict(value = AppConstants.CACHE_GROUP.USER_SELECTOR, allEntries = true)
	    public UserRep updateUser(String userId, Boolean isActive) {
		  	UserRep userRep = userRepDao.findById(userId)
		            .orElseThrow(() -> new NotFoundDataException("UserId Tidak Ditemukan"));
		  	userRep.setIsActive(isActive);
			userRepDao.save(userRep);
			return userRep;
	    }
	    
	    @Cacheable(AppConstants.CACHE_GROUP.USER_SELECTOR)
	    public List<UserRep> getAllUser() {
	       return userRepDao.findAll();
	    }
	    
	    @Cacheable(AppConstants.CACHE_GROUP.USER_SELECTOR)
	    public List<UserRep> getAllActiveUser(Boolean isActive) {
	       return userRepDao.findAllActiveUser(isActive);
	    }
}
