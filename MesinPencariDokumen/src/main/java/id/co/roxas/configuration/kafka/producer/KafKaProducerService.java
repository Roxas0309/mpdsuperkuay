package id.co.roxas.configuration.kafka.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate; 
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import id.co.roxas.constant.AppConstants;
import id.co.roxas.dto.LoggerApplicationWs;

@Service
public class KafKaProducerService {
	private static final Logger logger = 
            LoggerFactory.getLogger(KafKaProducerService.class);
     
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    
 
    public void sendMessage(String message) 
    {
        logger.info(String.format("Message sent -> %s", message));
        kafkaTemplate.send(AppConstants.TOPIC_NAME, message);
    }
    
    public void sendMessageLogger(LoggerApplicationWs message) 
    {
    	kafkaTemplate.send(AppConstants.TOPIC_LOGGER, new Gson().toJson(message));
    }
}
