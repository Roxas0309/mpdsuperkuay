package id.co.roxas.configuration.cache.service;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import id.co.roxas.constant.AppConstants;
import id.co.roxas.dao.document.DocumentDtlDao;
import id.co.roxas.dao.document.DocumentHdrDao;
import id.co.roxas.dao.document.DocumentHdrNative;
import id.co.roxas.dao.query.QuerySelectorDocumentDao;
import id.co.roxas.dto.DocumentRequestDto;
import id.co.roxas.entity.document.DocumentDtl;
import id.co.roxas.entity.document.DocumentHdr;
import id.co.roxas.entity.query.QuerySelectorDocument;
import id.co.roxas.entity.user.UserRep;
import id.co.roxas.throwable.MultipleDataErrorException;
import id.co.roxas.throwable.NotFoundDataException;

@Service
public class DocumentCacheService {

		@Autowired
		private DocumentDtlDao documentDtlDao;
		
		@Autowired
		private DocumentHdrDao documentHdrDao;
			
	    @Autowired
	    private QuerySelectorDocumentDao  querySelectorDocumentDao;
	    
	    @Autowired
	    private DocumentHdrNative documentHdrNative;
	 
	    
	    @CacheEvict(value=AppConstants.CACHE_GROUP.DOCS_CACHE_SELECTOR, allEntries = true)
	    public DocumentRequestDto savePic(DocumentRequestDto document) {
	    	Date date = new Date();
	    	DocumentHdr documentHdr = new DocumentHdr();
	    	documentHdr.setCreatedDate(date);
	    	documentHdr.setFileName(documentHdrNative.getFinaleName(document.getFileName()));
	    	documentHdr.setIsSearchable(true);
	    	documentHdrDao.save(documentHdr);
	    	Long id = documentHdr.getId();
	    	document.setId(id);
	    	document.setFileName(documentHdr.getFileName());
	    	for (String word : document.getWordingPerline()) {
	    		DocumentDtl documentDtl = new DocumentDtl();
	    		documentDtl.setCreatedDate(date);
	    		documentDtl.setIdHdr(id);
	    		documentDtl.setIsSearchable(true);
	    		documentDtl.setWordingPerLine(word);
	    		documentDtlDao.save(documentDtl);
			}
	    	
	        return document;
	    }
	    
//	    @CacheEvict(value = AppConstants.CACHE_GROUP.USER_SELECTOR, allEntries = true)
//	    public UserRep updateUser(String userId, Boolean isActive) {
//		  	UserRep userRep = userRepDao.findById(userId)
//		            .orElseThrow(() -> new NotFoundDataException("UserId Tidak Ditemukan"));
//		  	userRep.setIsActive(isActive);
//			userRepDao.save(userRep);
//			return userRep;
//	    }
	    
//	    @Cacheable(AppConstants.CACHE_GROUP.USER_SELECTOR)
//	    public List<UserRep> getAllUser() {
//	       return userRepDao.findAll();
//	    }
//	    
	    @Cacheable(AppConstants.CACHE_GROUP.DOCS_CACHE_SELECTOR)
	    public List<String> getAllDocumentTitle() {
	       return documentHdrDao.getAllTitleDocument();
	    }
	    
//	    @CachePut(value="QuerySelectorDocument", key="#invId")
//	    public QuerySelectorDocument updateQueryDocs(QuerySelectorDocument inv, Long invId) {
//	    	QuerySelectorDocument query = querySelectorDocumentDao.findById(invId)
//	            .orElseThrow(() -> new NotFoundDataException("Id Tidak Ditemukan"));
//	       return querySelectorDocumentDao.save(inv);
//	    }
//	    
//	    @CacheEvict(value=AppConstants.CACHE_GROUP.DOCS_SELECTOR, key = "#userId")
//	    public QuerySelectorDocument savePic(QuerySelectorDocument querySelectorDocument, String userId) {
//	    	querySelectorDocument.setCreatedDate(new Date());
//	        querySelectorDocumentDao.save(querySelectorDocument);
//	        return querySelectorDocument;
//	    }
//
//
//	    @Cacheable(value=AppConstants.CACHE_GROUP.DOCS_SELECTOR, key = "#userId")
//	    public List<String> getAllQueryById(String userId) {
//	       return querySelectorDocumentDao.findQueryAllByUserAccessor(userId);
//	    }
}
