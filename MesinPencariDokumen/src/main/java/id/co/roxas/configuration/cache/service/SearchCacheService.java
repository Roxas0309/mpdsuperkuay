package id.co.roxas.configuration.cache.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import id.co.roxas.constant.AppConstants;
import id.co.roxas.dao.query.QuerySelectorDocumentDao;
import id.co.roxas.entity.query.QuerySelectorDocument;
import id.co.roxas.throwable.NotFoundDataException;


@Service
public class SearchCacheService {

	    @Autowired
	    private QuerySelectorDocumentDao  querySelectorDocumentDao;
	 
	    @CachePut(value="QuerySelectorDocument", key="#invId")
	    public QuerySelectorDocument updateQueryDocs(QuerySelectorDocument inv, Long invId) {
	    	QuerySelectorDocument query = querySelectorDocumentDao.findById(invId)
	            .orElseThrow(() -> new NotFoundDataException("Id Tidak Ditemukan"));
	       return querySelectorDocumentDao.save(inv);
	    }
	    
	    @CacheEvict(value=AppConstants.CACHE_GROUP.DOCS_SELECTOR, key = "#userId")
	    public QuerySelectorDocument savePic(QuerySelectorDocument querySelectorDocument, String userId) {
	    	querySelectorDocument.setCreatedDate(new Date());
	        querySelectorDocumentDao.save(querySelectorDocument);
	        return querySelectorDocument;
	    }

//	    @CacheEvict(value="_Query", key="#invId")
//	    // @CacheEvict(value="Invoice", allEntries=true) //in case there are multiple entires to delete
//	    public void deleteQuery(Integer invId) {
//	       Invoice invoice = invoiceRepo.findById(invId)
//	           .orElseThrow(() -> new InvoiceNotFoundException("Invoice Not Found"));
//	       invoiceRepo.delete(invoice);
//	    }

//	    @Cacheable(value="_Query", key="#invId")
//	    public Invoice getOneQuery(Integer invId) {
//	       Invoice invoice = invoiceRepo.findById(invId)
//	         .orElseThrow(() -> new InvoiceNotFoundException("Invoice Not Found"));
//	       return invoice;
//	    }

	    @Cacheable(value=AppConstants.CACHE_GROUP.DOCS_SELECTOR, key = "#userId")
	    public List<String> getAllQueryById(String userId) {
	       return querySelectorDocumentDao.findQueryAllByUserAccessor(userId);
	    }
	
}
