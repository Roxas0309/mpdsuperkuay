package id.co.roxas.dto;

import java.util.List;

public class DocumentRequestDto {
	private Long id;
	private String fileName;
	private List<String> wordingPerline;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public List<String> getWordingPerline() {
		return wordingPerline;
	}
	public void setWordingPerline(List<String> wordingPerline) {
		this.wordingPerline = wordingPerline;
	}
	
	
	
}
