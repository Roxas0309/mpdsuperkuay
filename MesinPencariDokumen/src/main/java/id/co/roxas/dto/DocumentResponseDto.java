package id.co.roxas.dto;

import java.math.BigInteger;

public class DocumentResponseDto {

	private String titleName;
	private String content;
	private BigInteger id;
	
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	public BigInteger getId() {
		return id;
	}
	public void setId(BigInteger id) {
		this.id = id;
	}

	
	
	
}
