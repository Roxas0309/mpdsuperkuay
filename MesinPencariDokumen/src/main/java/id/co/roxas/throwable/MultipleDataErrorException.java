package id.co.roxas.throwable;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FOUND)
public class MultipleDataErrorException extends RuntimeException{

	private static final long serialVersionUID = -7709761715178137895L;
	
	public MultipleDataErrorException(String message) {
		super(message);
	}
	
}
