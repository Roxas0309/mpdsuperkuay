package id.co.roxas.constant;

public class AppConstants {
	 	public static final String TOPIC_NAME = "test";
		public static final String TOPIC_LOGGER = "logger";
	    public static final String GROUP_ID = "group_id";
	    public interface CACHE_GROUP {
	    	public String DOCS_SELECTOR = "DOCS_SELECTOR";
	    	public String DOCS_CACHE_SELECTOR = "DOCS_CACHE_SELECTOR";
	    	public String USER_SELECTOR = "USER_SELECTOR";
	    }
}
