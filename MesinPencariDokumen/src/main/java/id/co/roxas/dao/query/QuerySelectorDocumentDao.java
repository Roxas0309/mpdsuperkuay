package id.co.roxas.dao.query;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.co.roxas.entity.query.QuerySelectorDocument;

@Repository
public interface QuerySelectorDocumentDao extends JpaRepository<QuerySelectorDocument, Long>{

	public List<String> findQueryAllByUserAccessor(String userId);
}
