package id.co.roxas.dao.document;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.roxas.entity.document.DocumentDtl;

public interface DocumentDtlDao extends JpaRepository<DocumentDtl, Long>{

}
