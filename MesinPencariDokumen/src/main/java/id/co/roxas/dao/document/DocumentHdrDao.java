package id.co.roxas.dao.document;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.roxas.entity.document.DocumentHdr;

@Repository
public interface DocumentHdrDao extends JpaRepository<DocumentHdr, Long>{

	@Query("select a.fileName from DocumentHdr a")
	public List<String> getAllTitleDocument();
	
	
	
}
