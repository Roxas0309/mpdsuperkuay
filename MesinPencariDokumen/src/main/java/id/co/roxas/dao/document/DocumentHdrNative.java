package id.co.roxas.dao.document;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import id.co.roxas.BaseCommon;

@Repository
@Transactional
@Service
public class DocumentHdrNative extends BaseCommon{

	@PersistenceContext
	private EntityManager entityManager;

	public String getFinaleName(String fileName) {
		int i = 0;
		String finalFileName = fileName;
		do {
		  if(i!=0) {
			  finalFileName = insertStringInBetweenTxt(fileName, "("+i+")");		
		  }
		  i++;
		  System.out.println("fileName sekarang = " + finalFileName);
		}while(isExistFileName(finalFileName));
		
		return finalFileName;
	}
	
	private Boolean isExistFileName(String fileName) {
		  String query = "SELECT id, created_date, file_name, is_searchable\n"
					+ "FROM document_hdr where file_name = '"+fileName+"'";
		 // System.out.println("query " + query);
		  Object obj = entityManager.createNativeQuery(query).getResultList();
		  System.out.println("obj = " + new Gson().toJson(obj));
		  List<Object> mapList = null;
		try {
			mapList = mapperJsonToListDto(new Gson().toJson(obj), Object.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  if(mapList==null || mapList.size()==0) {
			  return false;
		  }  
		  return true;
	}

	public String insertString(String originalString, String stringToBeInserted, int index) {
		String newString = new String();
		for (int i = 0; i < originalString.length(); i++) {
			newString += originalString.charAt(i);
			if (i == index) {
				newString += stringToBeInserted;
			}
		}
		return newString;
	}

	public String insertStringInBetweenTxt(String originalString, String stringToBeInserted) {
		int in = originalString.toLowerCase().indexOf(".txt");
		return insertString(originalString, stringToBeInserted, in-1);
	}

}
