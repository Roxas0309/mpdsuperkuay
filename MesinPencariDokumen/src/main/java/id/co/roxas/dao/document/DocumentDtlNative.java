package id.co.roxas.dao.document;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.logging.log4j.util.Strings;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import id.co.roxas.BaseCommon;
import id.co.roxas.constant.AppConstants;
import id.co.roxas.dto.DocumentResponseDto;
import id.co.roxas.testing.Combination;

@Repository
@Transactional
@Service
public class DocumentDtlNative extends BaseCommon{

	@PersistenceContext
	private EntityManager entityManager;

//	@Cacheable(AppConstants.CACHE_GROUP.DOCS_CACHE_SELECTOR)
	@Cacheable(value = AppConstants.CACHE_GROUP.DOCS_CACHE_SELECTOR, key="#_qTitle")
	public List<DocumentResponseDto> getTextBookByItsTitle(String _qTitle){
		 String query = "SELECT hdr.id, hdr.file_name ,string_agg(concat(wording_per_line , '\\n'),',' order by dtl.id desc) as value "
		 		+ " FROM document_dtl dtl "
		 		+ " inner join document_hdr hdr "
		 		+ " on dtl.id_hdr  = hdr.id "
		 		+ " where hdr.file_name like '%"+_qTitle+"%' or hdr.id = "+_qTitle+" "
		 		+ " group by hdr.file_name, hdr.id ";
		 @SuppressWarnings("unchecked")
		 List<Object[]> obj = entityManager.createNativeQuery(query).getResultList();
		 List<DocumentResponseDto> getDocResps = new ArrayList<DocumentResponseDto>();
		 for (Object[] ob : obj) {
			 DocumentResponseDto documentResponseDto = new DocumentResponseDto();
			 documentResponseDto.setId((BigInteger) ob[0]);
			 documentResponseDto.setTitleName((String) ob[1]);
			 documentResponseDto.setContent((String) ob[2]);
			 getDocResps.add(documentResponseDto);
		 }
		return getDocResps;
	}
	
	private List<String> componentString(String _qTitle) {
		String[] arrsString = _qTitle.split(" ");
		List<String> stripp= Arrays.asList(arrsString).stream().filter(a-> Strings.isNotBlank(a)).collect(Collectors.toList());
		
		List<String> resultAll = new ArrayList<String>();
		
		if(stripp.size()==0) {
			resultAll.add("");
		}else {
			resultAll = getallCombinationWording(stripp);
		}
		
		return resultAll;
	}
	
	private List<String> getallCombinationWording(List<String> qWording){
		List<List<Integer>> lint = new ArrayList<List<Integer>>();
		int arr[] = new int[qWording.size()] ;
		
		for(int i = qWording.size()-1; i>=0; i--) {
			arr[qWording.size()-i-1] = i;
		}
		
		
		
		List<String> fin = new ArrayList<String>();
		Combination.printAllCombinedArr(lint, arr, 1, qWording.size());
		for (List<Integer> li : lint) {
			String varLock = "";
			for (int i : li) {
				varLock += qWording.get(i) + " ";
			}
			fin.add(varLock.trim());
		}
		return fin;
	}
	
	@Cacheable(value = AppConstants.CACHE_GROUP.DOCS_CACHE_SELECTOR, key="#_qTitle")
	public List<String> getAllStringTitleWithQTitle(String _qTitle){
		
		if (_qTitle!=null) {
			_qTitle = _qTitle.trim();
		}
		
		String queryA = "";
	    List<String> arrsString = componentString(_qTitle);
	    System.out.println("arrsString : " + arrsString.size());
	    
	    int i = 1;
	    
		for (String arr : arrsString) {		
			queryA +=  " select a.*, '"+arr+"' as int_docs \n"
					+ " from (SELECT hdr.id as identitas ,hdr.file_name as nama_file ,string_agg(concat(wording_per_line , '\\n'),',' order by dtl.id desc) as text_book "
					+ " FROM document_dtl dtl "
					+ " inner join document_hdr hdr "
					+ " on dtl.id_hdr  = hdr.id "
					+ " group by hdr.file_name, hdr.id) a "
					+ " where "
					+ " upper(a.nama_file) like upper('%"+arr+"%') "
					+ " or "
					+ " upper(a.text_book) like upper('%"+arr+"%') "
					+ (arrsString.size()!= i? " Union " : "");	
			i++;
		}
	    
		queryA = "select count(b.nama_file) as counter_me, b.nama_file as file_me from "
				+ "( " + queryA + " ) b "
				+ "group by b.nama_file "
				+ "order by counter_me desc ";
		
		
		System.out.println("query selected  " + queryA);
		@SuppressWarnings("unchecked")
		List<Object[]> obj = entityManager.createNativeQuery(queryA).getResultList();
		List<String> strings = new ArrayList<String>();
		for (Object[] ob : obj) {
			strings.add((String) ob[1]);
		}
		return strings;
	}
	
}
