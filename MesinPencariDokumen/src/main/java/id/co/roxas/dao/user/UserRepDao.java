package id.co.roxas.dao.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.roxas.entity.user.UserRep;

@Repository
public interface UserRepDao extends JpaRepository<UserRep, String>{

	@Query("select a from UserRep a where a.username = ?1 and a.password = ?2")
	public UserRep findByUserNameAndPassword(String username,String password);
	
	@Query("select a from UserRep a where a.username = ?1 ")
	public UserRep findByUserId(String username);
	
	@Query("select a from UserRep a where a.isActive = ?1 ")
	public List<UserRep> findAllActiveUser(Boolean isActive);
	
}
