package id.co.roxas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.roxas.dao.user.UserRepDao;
import id.co.roxas.entity.user.UserRep;

@Service
public class AuthenticationSvc {

	@Autowired
	private UserRepDao userRepDao;
	
	public UserRep getUserRep(String password, String username) {
		return userRepDao.findByUserNameAndPassword(username, password);
	}
	
}
