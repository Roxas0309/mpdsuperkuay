package id.co.roxas.controller.document;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.roxas.configuration.cache.service.DocumentCacheService;
import id.co.roxas.dao.document.DocumentDtlNative;
import id.co.roxas.dao.document.DocumentHdrNative;
import id.co.roxas.dto.DocumentRequestDto;
import id.co.roxas.dto.DocumentResponseDto;

@RestController
@RequestMapping("/document")
public class DocumentController {

	@Autowired
	private DocumentCacheService documentCacheService;
	
	@Autowired
	private DocumentDtlNative documentDtlNative;
	
	@PostMapping("/saveDocs")
	public DocumentRequestDto saveDocs(@RequestBody DocumentRequestDto documentRequestDto) {
		return documentCacheService.savePic(documentRequestDto);
	}
	
	@GetMapping("/get-all-title")
	public List<String> getallTitle(){
		return documentCacheService.getAllDocumentTitle();
	}
	
	@GetMapping("/get-all-content")
	public List<DocumentResponseDto> getAllContent(@RequestParam String _qTitle){
		return documentDtlNative.getTextBookByItsTitle(_qTitle);
	}
	
	@GetMapping("/get-all-title-search")
	public List<String> getAllTitleSearch(@RequestParam String _qTitle){
		return documentDtlNative.getAllStringTitleWithQTitle(_qTitle);
	}
	
}
