package id.co.roxas.controller.searcher;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.roxas.configuration.cache.service.SearchCacheService;
import id.co.roxas.entity.query.QuerySelectorDocument;

@RestController
@RequestMapping("/search-document")
public class SearchDocumentFileCtl {

	@Autowired
	private SearchCacheService searchCacheService;
	
	@GetMapping("")
	public String getFileDocumentByGetData(@RequestParam("_q") String _q,Authentication authentication, HttpServletRequest httpServletRequest) {
		QuerySelectorDocument document = new QuerySelectorDocument();
		document.setQuery(_q);
		document.setUserAccessor(authentication.getName());
		searchCacheService.savePic(document,authentication.getName());
		return _q;
	}

	@GetMapping("/get-current-search")
	public List<String> getCurrentSearch(Authentication authentication) {
		return searchCacheService.getAllQueryById(authentication.getName());
	}
	
	
	
}
