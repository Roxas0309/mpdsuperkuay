package id.co.roxas.controller.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.roxas.configuration.cache.service.UserCacheService;
import id.co.roxas.entity.user.UserRep;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserCacheService userCacheService;
	
	@PostMapping("/create-user")
	public UserRep createUser(@RequestBody String userId) {
		UserRep userRep = userCacheService.savePic(userId);
		return userRep;
	}
	
	@PutMapping("/edit-activity")
	public UserRep editActivity(@RequestParam("userId") String userId, @RequestParam("isActive") Boolean isActive) {
		UserRep userRep = userCacheService.updateUser(userId, isActive);
		return userRep;
	}
	
	@GetMapping("/get-all-user")
	public List<UserRep> getAllUser(@RequestParam(name = "_activity", required = true) Boolean _activity, 
									@RequestParam(name = "isActive", required = false) Boolean isActive) {
		List<UserRep>userReps = new ArrayList<UserRep>(); 
		if(_activity) {
			userReps = userCacheService.getAllActiveUser(isActive);
		}
		else {
			userReps = userCacheService.getAllUser();
		}
		return userReps;
	}
	
}
