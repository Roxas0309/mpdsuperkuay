package id.co.roxas.testing;

//Java program to print all combination of size r in an array of size n
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class Combination {

	static void combinationUtil(List<List<Integer>> lint,int arr[], int data[], int start,
								int end, int index, int r)
	{
		if (index == r)
		{
			List<Integer> inte = new ArrayList<Integer>();
			for (int j=0; j<r; j++) {
				inte.add(data[j]);
			}
			lint.add(inte);
			return;
		}

		for (int i=start; i<=end && end-i+1 >= r-index; i++)
		{
			data[index] = arr[i];
			combinationUtil(lint,arr, data, i+1, end, index+1, r);
		}
	}

	static void printCombination(List<List<Integer>> lint,int arr[], int n, int r)
	{
		int data[]=new int[r];
		combinationUtil(lint,arr, data, 0, n-1, 0, r);
	}
	
	public static void printAllCombinedArr(List<List<Integer>> lint,int arr[],int boundaryStart, int boundaryEnd) {
		int counter = arr.length;
		while(counter!=0) {
			if(counter>=boundaryStart && counter<=boundaryEnd) {
			printCombination(lint, arr,  arr.length, counter);
			}
			counter--;
		}
	}

	
	public static void main (String[] args) {
		int arr[] = {1, 2, 3, 4, 5};
		int r = 3;
		int n = arr.length;
		
		List<List<Integer>> lint = new ArrayList<List<Integer>>();
		printAllCombinedArr(lint,arr,1,3);
		System.out.println("lint : " + new Gson().toJson(lint));
	}
}

/* This code is contributed by Devesh Agrawal */
