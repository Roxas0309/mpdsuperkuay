# Mesin Pencari Dokumen #

MPD atau mesin pencari dokumen adalah sebuah mini project dengan bentuk microservice yang dapat diakses melalui web service 
dengan kegunaan mencari judul text file dengan cara mencari potongan kata dari isi konten tersebut.

Adapun tools yang dipakai dalam pengembangan ini adalah sebagai berikut : 

* Spring Boot, web service, java 11
* Native Sql
* Docker Container
* Zookeper dan Kafka
* Cache Redis
* Spring OAuth JWT
* DB Postgres

Adapun fitur-fitur yang disediakan pada mini project ini adalah sebagai berikut :

1. User Login
2. Penambahan User
3. Perubahan Hak Akses
4. Search Document Berdasarkan Judul
5. Penyimpanan Document dan Contentnya
6. Search Document Berdasarkan Isi Contentnya
7. Lihat Isi Content Berdasarkan Judul atau id

Mohon Buka dokumentasi untuk melihat dan mendapatkan postman datanya dan isi database beserta contohnya.